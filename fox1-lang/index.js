/**
 *  使用方法
	import lang from '@/lang/index.js'
	let langObj = new lang('en');
	Vue.use(langObj);
 */
//语言包
let langPackage={
	'en':require('./language/en.json'),
	'zh':require('./language/zh.json'),
}

class Lang {
	//------------------------
	
	//------------------------
	constructor(lang='zh'){
		this.langPackage=langPackage
		this.lang=lang
	}
	//---------------  
	install(Vue) {
		let self=this;
		//翻译
		Vue.prototype.$lang=function(key){
			return self.langPackage[self.lang][key];
		}
		//修改语言
		Vue.prototype.$setlanguage=function(lang){
			self.lang=lang
		}
		
		//获取语言
		Vue.prototype.$getlanguage=function(){
			return self.lang
		}
	}
	
}


export default Lang

