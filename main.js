import App from './App'

// #ifndef VUE3
import Vue from 'vue'
import './uni.promisify.adaptor'
// import share from 'share.js'
import share from '@/mixins/share.js'
import lang from '@/fox1-lang/index.js'
// import VConsole from 'vconsole'
// Vue.mixin(share)
let langObj = new lang('zh');
Vue.use(langObj); 
Vue.config.productionTip = false
App.mpType = 'app'
const app = new Vue({
  ...App
})
// const vConsole = new VConsole()
app.$mount()
// app.mixin(share)
// #endif

// #ifdef VUE3
import { createSSRApp } from 'vue'
export function createApp() {
  const app = createSSRApp(App)
  return {
    app
  }
}
// #endif