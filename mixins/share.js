
export default {
	data() {
		return {}
	},
	//1.发送给朋友
	onShareAppMessage() {
    return {
      title: '数字力场VHOST主播工厂',                //分享的标题
      path: 'pages/index/index',      //点击分享链接之后进入的页面路径
      imageUrl: 'https://digit-person.oss-cn-beijing.aliyuncs.com/digital_cloth/1716706879120.png' ,//分享发送的链接图片地址
	  summary:'一站式数字人全栈解决方案专家，拥有数字人直播系统和数字人工厂创新互动平台。'
    };
  },
	//2.分享到朋友圈
	onShareTimeline(){
    return{
      title: '数字力场VHOST主播工厂',                //分享的标题
      path: 'pages/index/index',      //点击分享链接之后进入的页面路径
      imageUrl: 'https://digit-person.oss-cn-beijing.aliyuncs.com/digital_cloth/1716706879120.png' ,//分享发送的链接图片地址
	  summary:'一站式数字人全栈解决方案专家，拥有数字人直播系统和数字人工厂创新互动平台。'
    }
  },
}